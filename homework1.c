/* homework1.c */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define WORDLENGTH 32

typedef struct word{
  int alph[26];
  char moji[WORDLENGTH];
  struct word *next;
}word;

void setalphabet(word *kotoba){
  int i;
  for(i=0; i<26; i++){
    kotoba->alph[i] = 0;  // 値を初期化
  }
  for(i=0; i<WORDLENGTH; i++){
    kotoba->moji[i] = tolower(kotoba->moji[i]);  // 大文字を小文字に変換
    if((kotoba->moji[i] >= 0x16) && (kotoba->moji[i] <= 0x7A)){ // a ~ z
      // 小文字以外はカウントしない
      kotoba->alph[kotoba->moji[i]-0x61]++;  // 単語は小文字とする
    }
  }
  // 確認用
  /*
  for(i=0; i<26; i++){
    printf("%d", kotoba->alph[i]);
  }
  printf("\n");
  */
}

void setdictionary(word *koumoku, word *prev){
  int i;
  // ポインタを処理 リスト構造に
  koumoku->next = prev;

  // alph[]を記録
  setalphabet(koumoku);
}

int main(void){
  FILE* fp;
  word *tmp;
  word *prev = NULL;
  int i, j;
  char *flag;
  word inputword;
  char buffer[WORDLENGTH];
  int wrdlen;
  
  // テキストファイルを開く
  fp = fopen("/usr/share/dict/american-english","r");
  // 動作確認のため、小さめのテキストファイルで実行
  // fp = fopen("wordlist.txt","r");
  
  while(1){
    // 単語を一つとってきては、setdictionary()にいれる
    tmp = (word*)malloc(sizeof(word));  // 領域確保
    flag = fgets(tmp->moji, WORDLENGTH, fp);
    if(flag == NULL){  // 読めなかったら終了
      free(tmp);
      printf("I can't read more.\n");
      break;
    }
    //  printf(tmp->moji);  // 確認用
    tmp->moji[strlen(tmp->moji)-1]='\0';
    setdictionary(tmp, prev);
    prev = tmp;
  }


  // 確認用表示
  // printf("Done!\n");
  
  /*
  tmp = prev;
  while(tmp != NULL){
    printf("word: %s\n",tmp->moji);
    for(i=0; i<26; i++){
      printf("%d", tmp->alph[i]);
    }
    printf("\n");
    tmp = tmp->next;
  }
  */
  
  // 入力をうけつけ
  printf("Please input word : ");
  scanf("%s",inputword.moji);
  setalphabet(&inputword);

  // 確認用表示
  for(i=0; i<26; i++){
    printf("%d", inputword.alph[i]);
  }
  printf("\n");
  // 何度か実行し、moonstarerを入力するのを繰り返したがしたが、alph[]の値がおかしくなる場合も
  // dirtyroomも繰り返し入力したが、何回かに一回はalph[]の値が少しおかしくなった

  // 該当する言葉を出力
  tmp = prev;
  wrdlen = 0;
  printf("You can make these words:");
  while(tmp != NULL){
    for(i=0; i<26; i++){  // その言葉が作れるか判定
      if(tmp->alph[i] > inputword.alph[i]){
	break;
      }
    }
    if(i==26){ //　もし作れたら
      //  printf(" %s ", tmp->moji);
      // 一番長い言葉を選定、bufferに記録
      if(wrdlen < strlen(tmp->moji)){
	strcpy(buffer, tmp->moji);
	wrdlen = strlen(tmp->moji);
      }
      // 今は、同じ長さの言葉が見つかっても、最初に見つけた一つしか記録しないが、
      // 今後は複数記録できるようにしたい
    }    
    tmp = tmp->next;
  }
  printf("\n");
  // bufferには、一番長い言葉が記録されている
  printf("The longest word is : %s\n", buffer);  
  fclose(fp);
}
