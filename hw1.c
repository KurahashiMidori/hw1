/* hw1.c */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define WORDLENGTH 32

typedef struct word{
  int alph[26];
  char moji[WORDLENGTH];
  struct word *next;
}word;

void setalphabet(word *kotoba){
  int i;
  for(i=0; i<26; i++){
    kotoba->alph[i] = 0;  // 値を初期化
  }
  for(i=0; i<WORDLENGTH; i++){
    if(kotoba->moji[i] != 0x27){   //  ' は除く
      kotoba->moji[i] = tolower(kotoba->moji[i]);  // 大文字を小文字に変換
      kotoba->alph[kotoba->moji[i]-0x61]++;  // 単語は小文字とする
    }
  }
  // 確認用
  for(i=0; i<26; i++){
    printf("%d", kotoba->alph[i]);
  }
  printf("\n");
  // この時点ではalph[]は正しく入力されている
}

void setdictionary(word *koumoku, word *prev){
  int i;
  // ポインタを処理 リスト構造に
  koumoku->next = prev;

  // alph[]を記録
  setalphabet(koumoku);
}

int main(void){
  FILE* fp;
  word *tmp;
  word *prev = NULL;
  int i, j;
  char *flag;
  word inputword;
  
  // テキストファイルを開く
  // fp = fopen("/usr/share/dict/american-english","r");
  fp = fopen("test.txt","r");
  
  while(1){
    // 単語を一つとってきては、setdictionary()にいれる
    tmp = (word*)malloc(sizeof(word));  // 領域確保
    flag = fgets(tmp->moji, WORDLENGTH, fp);
    if(flag == NULL){  // 読めなかったら終了
      free(tmp);
      printf("I can't read more.\n");
      break;
    }
    printf(tmp->moji);  // 確認用
    tmp->moji[strlen(tmp->moji)-1]='\0';
    setdictionary(tmp, prev);
    prev = tmp;
  }


  // 確認用表示
  printf("Done!\n");
  
  tmp = prev;
  while(tmp != NULL){
    printf("word: %s\n",tmp->moji);
    for(i=0; i<26; i++){
      printf("%d", tmp->alph[i]);
    }
    // ここでalph[]の値を確認すると、xに当たる値がおかしくなっている
    printf("\n");
    tmp = tmp->next;
  }
  
  // 入力をうけつけ
  printf("Please input word : ");
  scanf("%s",inputword.moji);
  setalphabet(&inputword);

  // 該当する言葉を出力
  tmp = prev;
  while(tmp != NULL){
    for(i=0; i<26; i++){  // その言葉が作れるか判定
      if(tmp->alph[i] > inputword.alph[i]){   // ここで Segmentation fault
	break;
      }
    }
    if(i==26){ //　もし作れたら
      printf(" %s ", tmp->moji);
    }    
    tmp = tmp->next;
  }
  
  printf("\n");
  
  fclose(fp);
}
